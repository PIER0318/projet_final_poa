package blackjackgame;

import controller.Dealer;
import controller.StandardBlackJackRulesEvaluator;
import model.*;
import view.View;


public class Main {

	public static void main(String[] args) throws Exception {
		
		Dealer gc = new Dealer(new Deck(), new View(), new StandardBlackJackRulesEvaluator(), new RoguePlayer("Bernie Madoff"));
		gc.run();
		
	}
}
