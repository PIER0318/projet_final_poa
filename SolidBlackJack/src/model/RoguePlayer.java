package model;

public class RoguePlayer extends Player {
	
	//--- attributes ---
	private String name;
	
	
	/**
	* Constructor of the RoguePlayer class.
	*/
	
	public RoguePlayer() {
		super();
		this.name = "inconnu";
	}
	
	
	/**
	* Another constructor of the RoguePlayer class.
	* @param name : Name of the rogue player.
	*/
	
	public RoguePlayer(String name) {
		super();
		this.name = name;
	}
	
	
	/**
	* Allows to set the name of the rogue player at the beginning of the game.
	* @param name : Name of the rogue player.
	*/
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	* Getter of the name attribute.
	* @return name : Name of the rogue player.
	*/
	
	public String getName(String name) {
		return this.name;
	}
}
