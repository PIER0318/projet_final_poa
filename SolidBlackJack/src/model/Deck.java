package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Deck {
	
	// --- attributes ---
	private ArrayList<Card> cards;
	

	/**
	* Constructor of the Deck class. Allows to build a deck of 52 cards and shuffle them.
	*/
	
	public Deck() {
		//instantiation of the card list
		this.cards = new ArrayList<Card>();
		//creation of the 52 cards
		for(Value cardValue : Value.values()) {	
			for(Suit cardSuit : Suit.values()) {
				this.cards.add(new Card(cardValue, cardSuit)); //add a card to the deck
			}
		}
		this.shuffle();
	}
	
	
	/**
	* Allows to shuffle the cards in the deck.
	*/
	
	public void shuffle() {
		Random random = new Random();
		for(int i = 0; i < this.cards.size(); i++) {
			Collections.swap(this.cards, i, random.nextInt(this.cards.size()));
		}
	}

	
	/**
	* Allows to draw a card from the deck.
	* @return card : A card.
	*/
	
	public Card draw() {
		return this.cards.remove(0);
	}
	
	
	/**
	* Allows to place back a card in the deck.
	* @param addCard : A card.
	*/
	
	public void addCard(Card addCard) {
		this.cards.add(addCard);
	}
	
	
	/**
	* Converts the deck into a string.
	* @return string : A message containing the value and suit of a all the cards in the deck.
	*/
	
	@Override
	public String toString() {
		String deckStr = "";
		int i = 1;
		for(Card iCard : this.cards) {
			deckStr += i + ": " + iCard.toString() + "\n";
			i ++;
		}
		return deckStr;
	}
	
}
