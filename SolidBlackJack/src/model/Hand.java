package model;

import java.util.ArrayList;

public class Hand {
	
	//--- attributes ---
	private ArrayList<Card> cards;
	

	/**
	* Constructor of the Hand class.
	*/
	
	public Hand() {
		this.cards = new ArrayList<Card>();
	}
	

	/**
	* Allows to add a card to the hand.
	* @param card : A card.
	*/
	
	public void addCard(Card card) {
		this.cards.add(card);
	}
	
	
	/**
	* Getter of the card parameter.
	* @return card : A card.
	*/
	
	public Card getCard(int i) {
		return this.cards.get(i);
	}
	
	
	/**
	* Allows to remove a card from the hand.
	* @return card : A card.
	*/
	
	public Card removeCard() {
		return cards.remove(0);
	}
	
	
	/**
	* Another getter of the card parameter (allows to get all the cards from the hand).
	* @return cards : ArrayList of cards.
	*/
	
	public ArrayList<Card> getCards(){
		return this.cards;
	}
	

	/**
	* Converts the hand into a string.
	* @return string : A message containing the value and suit of a all the cards in the hand.
	*/
	
	public String toString() {
		String handStr = "";
		for(Card iCard : this.cards) {
			handStr += iCard.toString() + " ";
		}
		return handStr;
	}
	
}
