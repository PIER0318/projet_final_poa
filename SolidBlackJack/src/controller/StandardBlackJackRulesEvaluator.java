package controller;

import model.Card;
import model.Hand;

public class StandardBlackJackRulesEvaluator implements GameEvaluator {
	
	/**
	* This method allows to calculate the value of a hand using standard blackjack rules.
	* @param hand	  : A hand (either the player or the dealer).
	* @return integer : Value of the hand passed in parameters.
	*/
	
	@Override
	public int evaluateHandValue(Hand hand) {
		int handValue = 0;
		int acesNb = 0;
		for(Card iCard : hand.getCards()) {
			switch(iCard.getValue()) {
			case TWO: handValue += 2; break;
			case THREE: handValue += 3; break;
			case FOUR: handValue += 4; break;
			case FIVE: handValue += 5; break;
			case SIX: handValue += 6; break;
			case SEVEN: handValue += 7; break;
			case EIGHT: handValue += 8; break;
			case NINE: handValue += 9; break;
			case TEN: handValue += 10; break;
			case JACK: handValue += 10; break;
			case QUEEN: handValue += 10; break;
			case KING: handValue += 10; break;
			case ACE: acesNb += 1; break;
			}
		}
		for(int i = 0; i < acesNb; i++) {
			if(handValue > 10) {
				handValue += 1;
			}
			else {
				handValue += 11;
			}
		}
		return handValue;
	}

}
