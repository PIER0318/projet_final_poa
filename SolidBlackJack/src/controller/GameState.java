package controller;

public enum GameState {
	
	PLACING_BETS, DEALING_CARDS, PLAYING_ROUND, REVEALING_WINNER, END_OF_GAME,
	
}
