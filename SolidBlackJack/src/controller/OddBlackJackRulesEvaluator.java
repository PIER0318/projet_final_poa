package controller;

import model.Card;
import model.Hand;

public class OddBlackJackRulesEvaluator implements GameEvaluator {
	
	/**
	* This method allows to calculate the value of a hand using weird rules.
	* @param hand	  : A hand (either the player or the dealer).
	* @return integer : Value of the hand passed in parameters.
	*/
	
	@Override
	public int evaluateHandValue(Hand hand) {
		int handValue = 0;
		for(Card iCard : hand.getCards()) {
			switch(iCard.getSuit()) {
			case HEARTS: handValue += 1; break;
			case DIAMONDS: handValue += 5; break;
			case SPADES: handValue += 10; break;
			case CLUBS: handValue += 15; break;
			}
		}
		return handValue;
	}

}
