package controller;

import model.Deck;
import model.Hand;
import model.Player;
import view.View;


public class Dealer {
	
	// --- attributes ---
	GameState gameState;
	Player player;
	Hand hand;
	Deck deck;
	View view;
	int playerBet;
	GameEvaluator evaluator;
	

	/**
	* Constructor of the Dealer class.
	*/
	
	public Dealer(Deck deck, View view, GameEvaluator evaluator, Player player){
		this.view = view;
		this.deck = deck;
		this.player = player;
		this.hand = new Hand();
		this.playerBet = 0;
		this.evaluator = evaluator;
		this.gameState = GameState.PLACING_BETS;
	}
	

	/**
	* This method is the main method that allows to switch between the states of the game and call corresponding functions.
	*/
	
	public void run() {
		switch(this.gameState) {
		case PLACING_BETS:
			this.placingBets();
			break;
		case DEALING_CARDS:
			this.dealingCards();
			break;
		case PLAYING_ROUND:
			this.playingRound();
			break;
		case REVEALING_WINNER:
			this.revealingWinner();
			break;
		case END_OF_GAME:
			this.endOfGame();
			break;
		}
	}
	
	
	/**
	* This method allows to register bets at the beginning of a round.
	*/
	
	public void placingBets() {
		int playerMoney = this.player.getMoney();
		
		if(playerMoney > 0) {
			playerBet = this.view.promptForBet(playerMoney);
			while (playerBet > playerMoney) {
				this.view.showBetErrorMsg();
				playerBet = this.view.promptForBet(playerMoney);
			}
			if(playerBet > 0) {
				this.view.showEndOfBetMsg();
				this.gameState = GameState.DEALING_CARDS;
			}
			else {
				this.gameState = GameState.END_OF_GAME;
			}
		}
		else {
			this.gameState = GameState.END_OF_GAME;
		}
		this.run();	
	}	
	
	
	/**
	* This method allows to deal the card at the beginning of a round. There are two for the player and two
	* for the dealer (one face up and one face down).
	*/
	
	public void dealingCards() {
		this.deck.shuffle();
		this.player.addCardToHand(this.deck.draw());
		this.player.addCardToHand(this.deck.draw());
		this.hand.addCard(this.deck.draw());
		this.hand.addCard(this.deck.draw());
		this.hand.getCard(1).flip();
		this.view.showPlayerCards(this.player.getHand());
		this.view.showDealerCards(this.hand);
		this.gameState = GameState.PLAYING_ROUND;
		this.run();
	}
	
	
	/**
	* This method allows the player to draw cards until he decides to stop or exceeds 21. Then, the dealer does the same
	* until he exceeds 17.
	*/
	
	public void playingRound() {
		int draw = 1;
		while(draw == 1 && this.evaluator.evaluateHandValue(this.player.getHand()) <= 21) { 
			draw = this.view.promptForDraw();
			if(draw == 1) {
				this.player.addCardToHand(this.deck.draw());
				this.view.showPlayerCards(this.player.getHand());
			}
		}
		while(this.evaluator.evaluateHandValue(this.hand) <= 17) {
			this.hand.addCard(this.deck.draw());
		}
		this.gameState = GameState.REVEALING_WINNER;
		this.run();
	}
	
	
	/**
	* This method calls the evaluator and checks for the winner.
	*/
	
	public void revealingWinner() {
		int dealerHandValue = this.evaluator.evaluateHandValue(this.hand);
		int playerHandValue = this.evaluator.evaluateHandValue(this.player.getHand());
		if(playerHandValue > 21) {
			this.view.showPlayerBust(playerHandValue);
			this.player.loseMoney(this.playerBet);
		}
		else if(dealerHandValue > 21) {
			this.view.showDealerHandValue(dealerHandValue);
			this.view.showDealerBust(dealerHandValue);
			try {
				this.player.winMoney(this.playerBet);
			} catch (Exception e) {
				System.err.println(e.getMessage());
				System.exit(1);	
			}
		}
		else {
			this.view.showPlayerHandValue(playerHandValue);
			this.view.showDealerHandValue(dealerHandValue);
			if(playerHandValue > dealerHandValue) {
				this.view.showPlayerWin();
				try {
					this.player.winMoney(this.playerBet);
				} catch (Exception e) {
					System.err.println(e.getMessage());
					System.exit(1);	
				}
			}
			else {
				this.view.showDealerWin();
				this.player.loseMoney(this.playerBet);
			}
		}
		this.cardsBackToDeck(this.hand);
		this.cardsBackToDeck(this.player.getHand());
		this.gameState = GameState.PLACING_BETS;
		this.run();
	}
	
	
	/**
	* This method updates the player's money according to bets and the winner previously revealed.
	*/
	
	public void endOfGame() {
		int playerMoney = this.player.getMoney();
		if(playerMoney > 0) {
			this.view.showRemainingMoney(playerMoney);
		}
		else {
			this.view.showPlayerBroke();
		}
		
	}
	
	
	/**
	* This method allows to remove the cards from a hand and put them back into the deck.
	* @param hand : Instance of Hand of the player or the dealer.
	*/
	
	public void cardsBackToDeck(Hand hand) {
		while(!hand.getCards().isEmpty()) {
			this.deck.addCard(hand.removeCard());
		}
	}
	
}
