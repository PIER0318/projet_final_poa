package controller;

import model.Hand;

public interface GameEvaluator {
	
	public int evaluateHandValue(Hand hand);
	
}
