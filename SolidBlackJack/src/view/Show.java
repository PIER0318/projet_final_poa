package view;

import model.Hand;

public interface Show {
	public void showBetErrorMsg();
	public void showEndOfBetMsg();
	public void showPlayerCards(Hand playerHand);
	public void showDealerCards(Hand dealerHand);
	public void showPlayerBust(int playerHandValue);
	public void showDealerBust(int dealerHandValue);
	public void showPlayerHandValue(int playerHandValue);
	public void showDealerHandValue(int dealerHandValue);
	public void showPlayerWin();
	public void showDealerWin();
	public void showRemainingMoney(int playerMoney);
	public void showPlayerBroke();
}


