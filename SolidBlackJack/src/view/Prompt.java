package view;

public interface Prompt {
	public int promptForBet(int playerMoney);
	public int promptForDraw();
}
