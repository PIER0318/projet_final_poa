package view;

import java.util.Scanner;
import model.Hand;

public class View implements Prompt, Show{
	Scanner scanner; 
	
	/**
	 * Constructor of the View class.
	 */
	
	public View() {
		this.scanner = new Scanner(System.in);
	}
	

	/**
	* This methods allows to prompt for the bets at the beginning of a round.
	* @param playerMoney : Player's current money. 
	* @return integer    : The amount bet by the player.
	*/
	
	@Override
	public int promptForBet(int playerMoney) {
		System.out.println("You have " + playerMoney + ", how much do you wanna bet?");
		return this.scanner.nextInt();
	}
	
	
	/**
	* This method allows to show an error message if the bet exceed the player's money.
	*/
	
	@Override
	public void showBetErrorMsg() {
		System.out.println("Not enough money!");
	}
	
	
	/**
	* This method allows to show a message at the end of bets phase.
	*/
	
	@Override
	public void showEndOfBetMsg() {
		System.out.println("End of bets!\n");
	}
	
	
	/**
	* This method allows to display cards of the player.
	* @param playerHand : Hand of the player.
	*/

	@Override
	public void showPlayerCards(Hand playerHand) {
		System.out.println("Your Hand: " + playerHand.toString());
	}
	
	
	/**
	* This method allows to display cards of the dealer.
	* @param dealerHand : Hand of the dealer.
	*/
	
	@Override
	public void showDealerCards(Hand dealerHand) {
		System.out.println("Dealer's Hand: " + dealerHand.toString());
	}
	

	/**
	* This method allows to ask the player for an action during a round. He can ever draw a card or pass his turn (stand).
	*/
	
	@Override
	public int promptForDraw() {
		System.out.println("Do you want to [1] Hit or [2] Stand?");
		return this.scanner.nextInt();
	}


	/**
	* This method allows to show a message when the player's hand value exceeds 21 (bust).
	* @param playerHandValue : Current value of the player's hand.
	*/
	
	@Override
	public void showPlayerBust(int playerHandValue){
		System.out.println("Player bust!\nYou lose!\n");
	}


	/**
	* This method allows to show a message when the dealer's hand value exceeds 21 (bust).
	* @param dealerHandValue : Current value of the dealer's hand.
	*/
	
	@Override
	public void showDealerBust(int dealerHandValue) {
		System.out.println("Dealer bust!\n");
	}

	
	/**
	* This method allows to display a message giving the current value of the player's hand.
	* @param playerHandValue : Current value of the player's hand.
	*/
	
	@Override
	public void showPlayerHandValue(int playerHandValue) {
		System.out.println("Your hand is valued at: " + playerHandValue);
	}
	
	
	/**
	* This method allows to display a message giving the current value of the dealer's hand.
	* @param dealerHandValue : Current value of the dealer's hand.
	*/
	
	@Override
	public void showDealerHandValue(int dealerHandValue) {
		System.out.println("Dealer's hand is valued at: " + dealerHandValue);
	}
	
	
	/**
	* This method allows to display a message when the player wins.
	*/
	
	@Override
	public void showPlayerWin() {
		System.out.println("You win!\n");
	}
	
	
	/**
	* This method allows to display a message when the dealer wins.
	*/
	
	@Override
	public void showDealerWin() {
	System.out.println("You lose!");
	}
	

	/**
	* This method allows to display a message indicating the remaining money of the player at the end of a turn.
	*/
	
	@Override
	public void showRemainingMoney(int playerMoney) {
		System.out.println("Your remaining money is: " + playerMoney +"\nHave a nice day!\n");
	}
	

	/**
	* Displays a message when the player has no money left.
	*/
	
	@Override
	public void showPlayerBroke(){
		System.out.println("You are broke!\ngame lost :(\n");
	}
}
