# 8INF957 - Projet Final : "Impact des Pincipes SOLID sur la Qualité du Code Orienté Objet"

---

## Description

L’objectif de ce rapport est de présenter une méthode chiffrée et sourcée d’évaluation de l’impact des principes SOLID 
sur la qualité du code orienté objet à l’aide des métriques CKJM [1]. Ces métriques ont été calculées sur deux versions 
d’un même code de blackjack simplifié, l’une respectant les principes SOLID et l’autre non. Ainsi, les résultats obtenus
ont montré que les valeurs des métriques étaient diminuées du passage du code non-SOLID au code SOLID, traduisant une 
amélioration de la qualité. En particulier, l’analyse de ces métriques prises séparément nous a permis de comprendre 
quels aspects recouverts par la notion de qualité étaient les plus impactés par les principes. Notre démarche est 
toutefois critiquable de par l’utilisation de deux versions d’un même code seulement ayant été réalisées l’une par 
rapport à l’autre ce qui ne garantit pas le respect ou non respect strict des principes. On pourrait envisager 
l’obtention de meilleurs résultats, plus robustes et se prêtant mieux au jeu d’une étude statistique avec des codes plus 
nombreux et/ou plus complets.


---

## Utilisation 

Ce dossier contient les répertoires sources de deux versions d'un même code de blackjack simplifié. La première est 
SOLID et la deuxième non. Pour l'exécution, il suffit d'ouvrir l'un des deux répertoires à l'aide d'un IDE come Eclipse 
ou Intellij et d'en effectuer la compilation. Ensuite, lancez l'execution sur la classe Main.

---

## Auteurs

- Pierre Lefebvre (LEFP18039809)
- Sophie Germain (GERS09059909)

---

## Sources

- [1] Chidamber, S., & Kemerer, C. (1994), A metrics suite for object oriented design. IEEE Transactions on Software Engineering, 20(6), 476-493. https://doi.org/10.1109/32.295895 .
- [2] Mcheick, H.  (2021), Abstraction de données et orientation objet Rappel [Diapositives], (1-6).
- [3] Mcheick, H. (2021), Programmation Objet Avancée - Introduction [Diapositives], (32-51).
- [4] McCabe, T. J. (1976), A Complexity Measure. IEEE Transactions on Software Engineering, 2(4), 308-320, doi: https://doi.org/10.1109/TSE.1976.233837 .
- [5] Spinellis, D. (2014), ckjm - Chidamber and Kemerer Java Metrics. https://www.spinellis.gr/sw/ckjm/ 
- [6] Leijdekkers, B. (2021, 14 mars). GitHub - BasLeijdekkers/MetricsReloaded: Automated source code metrics plugin for IntelliJ IDEA. GitHub. https://github.com/BasLeijdekkers/MetricsReloaded .
- [7] Lorraine Le Jan, L. J. (2021, 28 avril). Écrivez du code Java maintenable avec MVC et SOLID. OpenClassrooms.com. https://openclassrooms.com/fr/courses/6810956-ecrivez-du-code-java-maintenable-avec-mvc-et-solid
