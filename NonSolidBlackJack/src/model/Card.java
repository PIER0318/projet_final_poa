package model;

public class Card {
	
	// --- attributes ---
	private Suit suit;
	private Value value;
	private boolean flipped;
	

	/**
	* Constructor of the Card class. 
	*/
	
	public Card(Value value, Suit Suit) {
		this.value = value;
		this.suit = Suit;
		this.flipped = false;
	}
	
	
	/**
	* Getter of the value parameter. 
	* @return value : The value of a card.
	*/
	
	public Value getValue() {
		return this.value;
	}
	
	
	/**
	* Getter of the suit parameter.
	* @return suit : The suit (figure or number) of a card.
	*/
	
	public Suit getSuit() {
		return this.suit;
	}
	
	
	/**
	* Getter of the flipped parameter.
	* @return flipped : true if a card is flipped, false otherwise.
	*/
	
	public boolean isFlipped() {
		return this.flipped;
	}
	
	
	/**
	* Allows to flip a card.
	*/
	
	public void flip() {
		this.flipped = !this.flipped;
	}
	
	
	/**
	* Converts the cart into a string.
	* @return string : A message containing the value and suit of a card.
	*/
	
	@Override
	public String toString() {
		if(this.flipped == false) {
			return "[" + this.value.toString().toLowerCase() 
					+ " of " + this.suit.toString().toLowerCase() + "]";
		}
		else return "[face down]";
	}
	
}
