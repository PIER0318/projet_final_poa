package model;

public class Player {
	private int money;
	private Hand hand;
	
	/**
	* Constructor of the Player class.
	*/
	
	public Player() {
		this.money = 200;
		this.hand = new Hand();
	}
	
	
	/**
	* Allows to add card to the player's hand.
	* @param card : A card.
	*/
	
	public void addCardToHand(Card card) {
		hand.addCard(card);
	}
	
	
	/**
	* Allows to get the hand of the player.
	* @return hand : A hand.
	*/
	
	public Hand getHand() {
		return this.hand;
	}
	
	
	/**
	* Allows to get player's money.
	* @return money : an integer corresponding to the current amount of money of the player.
	*/
	
	public int getMoney() {
		return this.money;
	}
	
	
	/**
	* Allows to give money to the player
	* @param prize : Amount of money won by the player.
	*/
	
	public void winMoney(int prize) throws Exception {
		this.money +=  prize;
	}
	
	
	/**
	* Allows to get money from the player
	* @param loss : Amount of money lost by the player.
	*/
	
	public void loseMoney(int loss) {
		this.money -= loss;
	}

}
