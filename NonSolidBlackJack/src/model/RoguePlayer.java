package model;

public class RoguePlayer extends Player {
	
	//--- attributes ---
	private String name;
	
	
	/**
	* Constructor of the RoguePlayer class.
	*/
	
	public RoguePlayer() {
		super();
		this.name = "inconnu";
	}
	
	
	/**
	* Another constructor of the RoguePlayer class.
	* @param name : Name of the rogue player.
	*/
	
	public RoguePlayer(String name) {
		super();
		this.name = name;
	}
	
	
	/**
	* Allows to set the name of the rogue player at the beginning of the game.
	* @param name : Name of the rogue player.
	*/
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	* Getter of the name attribute.
	* @return name : Name of the rogue player.
	*/
	
	public String getName(String name) {
		return this.name;
	}
	
	
	/**
	* Override of the winMoney method. Allows to give money to the rogue player. If the amount exceeds 100$, the rogue player is 
	* found and the method throws an exception.
	* @param prize : Money won by the rogue player.
	*/
	
	@Override
	public void winMoney(int prize) throws Exception {
		super.winMoney(prize);
		if (prize >= 100) {
			throw new Exception("You've been spotted...");
		}
	}
}
