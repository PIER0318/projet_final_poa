package controller;

import java.util.Scanner;

import model.Deck;
import model.Hand;
import model.Player;


public class Dealer implements DealerInterface {
	
	// --- attributes ---
	GameState gameState;
	Player player;
	Hand hand;
	Deck deck;
	int playerBet;
	StandardBlackJackRulesEvaluator standard_evaluator;
	OddBlackJackRulesEvaluator odd_evaluator;
	Scanner scanner; 
	

	/**
	* Constructor of the Dealer class.
	*/
	
	public Dealer(Deck deck, StandardBlackJackRulesEvaluator standard_evaluator, OddBlackJackRulesEvaluator odd_evaluator, Player player){
		this.deck = deck;
		this.player = player;
		this.hand = new Hand();
		this.playerBet = 0;
		this.standard_evaluator = standard_evaluator;
		this.odd_evaluator = odd_evaluator;
		this.gameState = GameState.PLACING_BETS;
		this.scanner = new Scanner(System.in);
	}
	

	/**
	* This method is the main method that allows to switch between the states of the game and call corresponding functions.
	*/
	
	@Override
	public void run() {
		switch(this.gameState) {
		case PLACING_BETS:
			this.placingBets();
			break;
		case DEALING_CARDS:
			this.dealingCards();
			break;
		case PLAYING_ROUND:
			this.playingRound();
			break;
		case REVEALING_WINNER:
			this.revealingWinner();
			break;
		case END_OF_GAME:
			this.endOfGame();
			break;
		}
	}
	
	
	/**
	* This method allows to register bets at the beginning of a round.
	*/
	
	@Override
	public void placingBets() {
		int playerMoney = this.player.getMoney();
		
		if(playerMoney > 0) {
			playerBet = this.promptForBet(playerMoney);
			while (playerBet > playerMoney) {
				this.showBetErrorMsg();
				playerBet = this.promptForBet(playerMoney);
			}
			if(playerBet > 0) {
				this.showEndOfBetMsg();
				this.gameState = GameState.DEALING_CARDS;
			}
			else {
				this.gameState = GameState.END_OF_GAME;
			}
		}
		else {
			this.gameState = GameState.END_OF_GAME;
		}
		this.run();	
	}	
	
	
	/**
	* This method allows to deal the card at the beginning of a round. There are two for the player and two
	* for the dealer (one face up and one face down).
	*/
	
	@Override
	public void dealingCards() {
		this.deck.shuffle();
		this.player.addCardToHand(this.deck.draw());
		this.player.addCardToHand(this.deck.draw());
		this.hand.addCard(this.deck.draw());
		this.hand.addCard(this.deck.draw());
		this.hand.getCard(1).flip();
		this.showPlayerCards(this.player.getHand());
		this.showDealerCards(this.hand);
		this.gameState = GameState.PLAYING_ROUND;
		this.run();
	}
	
	
	/**
	* This method allows the player to draw cards until he decides to stop or exceeds 21. Then, the dealer does the same
	* until he exceeds 17.
	*/
	
	@Override
	public void playingRound() {
		int draw = 1;
		while(draw == 1 && this.standard_evaluator.evaluateHandValue(this.player.getHand()) <= 21) { 
			draw = this.promptForDraw();
			if(draw == 1) {
				this.player.addCardToHand(this.deck.draw());
				this.showPlayerCards(this.player.getHand());
			}
		}
		while(this.standard_evaluator.evaluateHandValue(this.hand) <= 17) {
			this.hand.addCard(this.deck.draw());
		}
		this.gameState = GameState.REVEALING_WINNER;
		this.run();
	}
	
	
	/**
	* This method calls the evaluator and checks for the winner.
	*/
	
	@Override
	public void revealingWinner() {
		int dealerHandValue = this.standard_evaluator.evaluateHandValue(this.hand);
		int playerHandValue = this.standard_evaluator.evaluateHandValue(this.player.getHand());
		if(playerHandValue > 21) {
			this.showPlayerBust(playerHandValue);
			this.player.loseMoney(this.playerBet);
		}
		else if(dealerHandValue > 21) {
			this.showDealerHandValue(dealerHandValue);
			this.showDealerBust(dealerHandValue);
			try {
				this.player.winMoney(this.playerBet);
			} catch (Exception e) {
				System.err.println(e.getMessage());
				System.exit(1);	
			}
		}
		else {
			this.showPlayerHandValue(playerHandValue);
			this.showDealerHandValue(dealerHandValue);
			if(playerHandValue > dealerHandValue) {
				this.showPlayerWin();
				try {
					this.player.winMoney(this.playerBet);
				} catch (Exception e) {
					System.err.println(e.getMessage());
					System.exit(1);	
				}
			}
			else {
				this.showDealerWin();
				this.player.loseMoney(this.playerBet);
			}
		}
		this.cardsBackToDeck(this.hand);
		this.cardsBackToDeck(this.player.getHand());
		this.gameState = GameState.PLACING_BETS;
		this.run();
	}
	
	
	/**
	* This method updates the player's money according to bets and the winner previously revealed.
	*/
	
	@Override
	public void endOfGame() {
		int playerMoney = this.player.getMoney();
		if(playerMoney > 0) {
			this.showRemainingMoney(playerMoney);
		}
		else {
			this.showPlayerBroke();
		}
		
	}
	
	
	/**
	* This method allows to remove the cards from a hand and put them back into the deck.
	* @param hand : Instance of Hand of the player or the dealer.
	*/
	
	@Override
	public void cardsBackToDeck(Hand hand) {
		while(!hand.getCards().isEmpty()) {
			this.deck.addCard(hand.removeCard());
		}
	}
	
	
	/**
	* This methods allows to prompt for the bets at the beginning of a round.
	* @param playerMoney : Player's current money. 
	* @return integer    : The amount bet by the player.
	*/
	
	@Override
	public int promptForBet(int playerMoney) {
		System.out.println("You have " + playerMoney + ", how much do you wanna bet?");
		return this.scanner.nextInt();
	}
	
	
	/**
	* This method allows to show an error message if the bet exceed the player's money.
	*/
	
	@Override
	public void showBetErrorMsg() {
		System.out.println("Not enough money!");
	}
	
	
	/**
	* This method allows to show a message at the end of bets phase.
	*/
	
	@Override
	public void showEndOfBetMsg() {
		System.out.println("End of bets!\n");
	}
	

	/**
	* This method allows to display cards of the player.
	* @param playerHand : Hand of the player.
	*/
	
	@Override
	public void showPlayerCards(Hand playerHand) {
		System.out.println("Your Hand: " + playerHand.toString());
	}
	
	
	/**
	* This method allows to display cards of the dealer.
	* @param dealerHand : Hand of the dealer.
	*/
	
	@Override
	public void showDealerCards(Hand dealerHand) {
		System.out.println("Dealer's Hand: " + dealerHand.toString());
	}
	

	/**
	* This method allows to ask the player for an action during a round. He can ever draw a card or pass his turn (stand).
	*/
	
	@Override
	public int promptForDraw() {
		System.out.println("Do you want to [1] Hit or [2] Stand?");
		return this.scanner.nextInt();
	}
	
	
	/**
	* This method allows to show a message when the player's hand value exceeds 21 (bust).
	* @param playerHandValue : Current value of the player's hand.
	*/
	
	@Override
	public void showPlayerBust(int playerHandValue){
		System.out.println("Player bust!\nYou lose!\n");
	}

	
	/**
	* This method allows to show a message when the dealer's hand value exceeds 21 (bust).
	* @param dealerHandValue : Current value of the dealer's hand.
	*/
	
	@Override
	public void showDealerBust(int dealerHandValue) {
		System.out.println("Dealer bust!\n");
	}
	
	
	/**
	* This method allows to display a message giving the current value of the player's hand.
	* @param playerHandValue : Current value of the player's hand.
	*/
	
	@Override
	public void showPlayerHandValue(int playerHandValue) {
		System.out.println("Your hand is valued at: " + playerHandValue);
	}
	
	
	/**
	* This method allows to display a message giving the current value of the dealer's hand.
	* @param dealerHandValue : Current value of the dealer's hand.
	*/
	
	@Override
	public void showDealerHandValue(int dealerHandValue) {
		System.out.println("Dealer's hand is valued at: " + dealerHandValue);
	}
	
	
	/**
	* This method allows to display a message when the player wins.
	*/
	
	@Override
	public void showPlayerWin() {
		System.out.println("You win!\n");
	}
	
	
	/**
	* This method allows to display a message when the dealer wins.
	*/
	
	@Override
	public void showDealerWin() {
		System.out.println("You lose!");
	}
	
	
	/**
	* This method allows to display a message indicating the remaining money of the player at the end of a turn.
	*/
	
	@Override
	public void showRemainingMoney(int playerMoney) {
		System.out.println("Your remaining money is: " + playerMoney +"\nHave a nice day!\n");
	}
	
	
	/**
	* Displays a message when the player has no money left.
	*/
	
	@Override
	public void showPlayerBroke(){
		System.out.println("You are broke!\ngame lost :(\n");
	}
	
}
