package controller;

import model.Hand;

public interface DealerInterface {

	// --- main routine ---
	void run();

	void placingBets();

	void dealingCards();

	void playingRound();

	void revealingWinner();

	void endOfGame();

	void cardsBackToDeck(Hand hand);

	// placing bets display
	int promptForBet(int playerMoney);

	void showBetErrorMsg();

	void showEndOfBetMsg();

	// dealing cards display
	void showPlayerCards(Hand playerHand);

	void showDealerCards(Hand dealerHand);

	// round display
	int promptForDraw();

	// revealing winner display
	void showPlayerBust(int playerHandValue);

	void showDealerBust(int dealerHandValue);

	void showPlayerHandValue(int playerHandValue);

	void showDealerHandValue(int dealerHandValue);

	void showPlayerWin();

	void showDealerWin();

	// end of game display
	void showRemainingMoney(int playerMoney);

	void showPlayerBroke();

}