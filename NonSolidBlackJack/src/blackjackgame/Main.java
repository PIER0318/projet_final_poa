package blackjackgame;

import controller.Dealer;
import controller.DealerInterface;
import controller.OddBlackJackRulesEvaluator;
import controller.StandardBlackJackRulesEvaluator;
import model.*;


public class Main {
	
	public static void main(String[] args) throws Exception {
		
		DealerInterface gc = new Dealer(new Deck(), new StandardBlackJackRulesEvaluator(), new OddBlackJackRulesEvaluator(), new RoguePlayer());
		gc.run();
		
	}
}
